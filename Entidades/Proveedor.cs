﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Entidades
{
    public class Proveedor
    {
        private int _idproveedor;
        private string _nombreproveedor;
        private string _direccion;
        private string _telefono;

        public int Idproveedor { get => _idproveedor; set => _idproveedor = value; }
        public string Nombreproveedor { get => _nombreproveedor; set => _nombreproveedor = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
    }
}
