﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;


namespace LogicaNegocios
{
    public class ProveedorManejador
    {
        private ProveedorAccesoDatos _proveedorAccesoDatos = new ProveedorAccesoDatos();

        public void Guardar(Proveedor proveedor)
        {
            _proveedorAccesoDatos.Guardar(proveedor);

        }
        public void Eliminar(int idproveedor)
        {
            //eliminar
            _proveedorAccesoDatos.Eliminar(idproveedor);
        }
        public List<Proveedor> GetProveedors(string filtro)
        {
            
            var listUsuario = _proveedorAccesoDatos.GetProveedors(filtro);
            //Llenar lista
            return listUsuario;
        }
    }
}
