﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace AccesoDatos
{
    class ConexionAccesoDatos
    {

        private MySqlConnection conn;

        public ConexionAccesoDatos(string servidor, string ususario, string passwords, string database, uint puerto)
        {
            MySqlConnectionStringBuilder cadenaConexion = new MySqlConnectionStringBuilder();
            cadenaConexion.Server = servidor;
            cadenaConexion.UserID = ususario;
            cadenaConexion.Password = passwords;
            cadenaConexion.Database = database;
            cadenaConexion.Port = puerto;

            conn = new MySqlConnection(cadenaConexion.ToString());
        }

        public void EjecutarConsulta(string consulta)
        {
            conn.Open();
            var command = new MySqlCommand(consulta, conn);
            command.ExecuteNonQuery();
            conn.Close();
        }

        public DataSet ObtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(consulta, conn);
            da.Fill(ds, tabla);
            return ds;
        }
    }
}

