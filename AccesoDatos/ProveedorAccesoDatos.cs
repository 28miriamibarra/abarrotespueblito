﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades;

namespace AccesoDatos
{
    public class ProveedorAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ProveedorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "tiendaabarrotes", 3306);
        }

        public void Guardar(Proveedor proveedor)
        {
            if (proveedor.Idproveedor == 0)
            {
                string consulta = string.Format("Insert into proveedor values(null,'{0}','{1}','{2}')", proveedor.Nombreproveedor, proveedor.Direccion, proveedor.Telefono);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update proveedor set nombreproveedor = '{0}', direccion ='{1}',telefono='{2}' where idproveedor= {3}", proveedor.Nombreproveedor, proveedor.Direccion, proveedor.Telefono, proveedor.Idproveedor);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idproveedor)
        {
            string consulta = string.Format("Delete from proveedor where idproveedor={0}", idproveedor);
            conexion.EjecutarConsulta(consulta);
        }


        public List<Proveedor> GetProveedors(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listproveedor = new List<Proveedor>();
            var ds = new DataSet();
            string consulta = "Select * from proveedor where nombreproveedor like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "proveedor");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var proveedor = new Proveedor
                {
                    Idproveedor = Convert.ToInt32(row["idproveedor"]),
                    Nombreproveedor = row["nombreproveedor"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                };

                listproveedor.Add(proveedor);
            }


            return listproveedor;
        }

    }
}
