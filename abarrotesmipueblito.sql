create database TiendaAbarrotes;
use tiendaabarrotes;

#region Tablas
create table categoria(
idcategoria int primary key auto_increment,
nombre varchar(50));

create table producto(
idproducto int primary key auto_increment,
nombre varchar(50),
precio double,
fkcategoria int,
foreign KEY (fkcategoria) references categoria(idcategoria));

create table proveedor(
idproveedor int primary key AUTO_INCREMENT,
nombreproveedor varchar(50),
direccion varchar (50),
telefono varchar (15));

create table compras(
idcompra int primary key auto_increment,
fkproveedor int,
fkproducto int,
fecha date,
cantidad double,
foreign key (fkproveedor) references proveedor (idproveedor),
foreign key (fkproducto) references producto (idproducto));

#End region
#region insertar
insert into categoria values(null,'Refrescos');
insert into categoria values(null,'Lacteos');
insert into categoria values(null,'Sabritas');
insert into categoria values(null,'Pan');
insert into categoria values(null,'Enlatados');
insert into categoria values(null,'Pastas');

insert into proveedor values( null, 'Juan Zerme�o', 'Lagos de Moreno', '4745378297');
insert into proveedor values( null, 'Oscar Torres', 'Paso de Cuarenta', '6482932028');
insert into proveedor values( null, 'Ulises Colunga', 'Lagos de Moreno', '4745348958');
insert into proveedor values( null, 'Rafael Martinez', 'Ojuelos', '2884628399');

insert into producto values(null, 'Coca-Cola 600ml', 14, 1);
insert into producto values(null, 'Doritos Nacho', 12, 3);
insert into producto values(null, 'Coctel de frutas', 35, 5);
insert into producto values(null, 'Yogurt Natural', 23, 2);
insert into producto values(null, 'Fresca 600 ml', 12, 1);

insert into compras values(null, 1, 1, '2020-02-12', 35);
insert into compras values(null, 2, 3, '2020-02-12', 10);
insert into compras values(null, 3, 4, '2020-02-12',25);

#end region
#region Vista
drop view v_venta;

create view v_venta as
select  idcompra as 'Folio', fecha as'Fecha', nombreproveedor as 'Proveedor', 
producto.nombre as 'Producto', cantidad as 'Cantidad' from compras, proveedor, producto
where compras.fkproveedor = proveedor.idproveedor
and compras.fkproducto = producto.idproducto;

select*from v_venta;
#end region 
#region Procedure
drop procedure if exists insertarcompra;
create procedure insertarcompra( 
in _idcompra int,
in _fkproveedor int, 
in _fkproducto int,
in _fecha date,
in _cantidad double,
out _mensaje varchar(50))
begin
if _cantidad = 0
  then
  select 'Inserte una cantidad' into _mensaje;
  else
  insert into compras values(null,_fkproveedor,_fkproducto ,_fecha,_cantidad); 
  end if;
end;

call insertarcompra(null,2,1,'2020-02-19',0,@ms);

select*from compras;
delete from compras where idcompra=5;
#end region
#region consulta

select producto.nombre, producto.precio, categoria.nombre from producto,categoria 
where producto.fkcategoria = categoria.idcategoria order by categoria.nombre ASC ;

SELECT*FROM PRODUCTO


#end region
