﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using LogicaNegocios;

namespace AbarrotesPueblito
{
    public partial class Form1 : Form
    {
        private ProveedorManejador _proveedorManejador;
        private Proveedor _proveedor;

        public Form1()
        {
            InitializeComponent();
            _proveedorManejador = new ProveedorManejador();
            _proveedor = new Proveedor();
        }

        private void CargarProveedor()
        {
            _proveedor.Idproveedor = Convert.ToInt32(lblId.Text);
            _proveedor.Nombreproveedor = txtNombre.Text;
            _proveedor.Direccion = txtDireccion.Text;
            _proveedor.Telefono = txtTelefono.Text;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BuscarProveedores("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtTelefono.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            lblId.Text = "0";
        }
        private void BuscarProveedores(string filtro)
        {
            dtgProveedor.DataSource = _proveedorManejador.GetProveedors(filtro);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarProveedor();
            if (true)
            {
                GuardarProveedor();
                LimpiarCuadros();
                BuscarProveedores("");
                ControlarBotones(true, false, false, true);
                ControlarCuadros(false);
            }
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtNombre.Focus();
        }
        private void GuardarProveedor()
        {
            _proveedorManejador.Guardar(_proveedor);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarProveedores(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarUsuario();
                    BuscarProveedores("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarUsuario()
        {
            var IdProveedor = dtgProveedor.CurrentRow.Cells["Idproveedor"].Value;
            _proveedorManejador.Eliminar(Convert.ToInt32(IdProveedor));
        }

        private void DtgProveedor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarProveedor();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarProveedor()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgProveedor.CurrentRow.Cells["Idproveedor"].Value.ToString();
            txtNombre.Text = dtgProveedor.CurrentRow.Cells["Nombreproveedor"].Value.ToString();
            txtDireccion.Text = dtgProveedor.CurrentRow.Cells["Direccion"].Value.ToString();
            txtTelefono.Text = dtgProveedor.CurrentRow.Cells["Telefono"].Value.ToString();
           
        }
    }
}
